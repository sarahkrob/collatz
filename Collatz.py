"""

Collatz project

"""

#!/usr/bin/env python3

# ----------
# Collatz.py
# ----------

from typing import IO, List

# ------------
# collatz_read
# ------------

def collatz_read(inp: str) -> List[int]:
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    spl = inp.split()
    return [int(spl[0]), int(spl[1])]

# ------------
# collatz_eval
# ------------

CYCLES = [0] * 1000000


def collatz_eval(vari: int, varj: int) -> int:
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    assert vari > 0 and varj > 0

    # assign first and last value in case numbers are out of order
    if vari > varj:
        first = varj
        last = vari
    else:
        first = vari
        last = varj

    cycletemp = 0
    maxcycle = 0
    half = (last // 2) + 1

    # no need to check the lower half of the numbers
    if first < half:
        first = half

    # iterate over range from first to last
    for i in range(first, last + 1):
        temp = i
        cycletemp = 1
        while temp > 1:
            savedcycle = 0
            # check to see if this cycle is already calculated
            if temp < 1000000:
                savedcycle = CYCLES[temp]
            # it is! we're done
            if savedcycle != 0:
                cycletemp += savedcycle - 1
                break
            # cycle has not been calculated
            else:
                if temp % 2 == 0:
                    temp = temp // 2
                else:
                    temp = (temp * 3) + 1
                cycletemp += 1
        # save calculated cycle
        CYCLES[i] = cycletemp
        # check if we have a new max
        if cycletemp > maxcycle:
            maxcycle = cycletemp

    assert maxcycle > 0
    return maxcycle

# -------------
# collatz_print
# -------------


def collatz_print(wrt: IO[str], begin: int, end: int, length: int) -> None:
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    wrt.write(str(begin) + " " + str(end) + " " + str(length) + "\n")

# -------------
# collatz_solve
# -------------


def collatz_solve(rdr: IO[str], wrt: IO[str]) -> None:
    """
    r a reader
    w a writer
    """
    for inp in rdr:
        i, j = collatz_read(inp)
        length = collatz_eval(i, j)
        collatz_print(wrt, i, j, length)
